; (C) 2021 Matt Heffernan 
; https://github.com/SlithyMatt/multi-mandlebrot
; Modification Apple IIe Uwe Gottschling 2022


i_result	equ $70 ; byte
xpos		equ $71
ypos		equ $72

Mandelbrot_Start
	ldx #0
	ldy #0
:loop
	jsr mand_get
	sta i_result
	stx xpos
	sty ypos
	jsr Plot
	ldx xpos
	ldy ypos
	inx
	cpx #MAND_WIDTH
	bne :loop
	ldx #0
	iny
	cpy #MAND_HEIGHT
	bne :loop
	rts
	
Plot
	lda i_result
	jsr SETCOL
	lda ypos
	ldy xpos
	jsr PLOT
	rts	




