; (C) 2021 Matt Heffernan 
; https://github.com/SlithyMatt/multi-mandlebrot
; Modification Oak65C02 Uwe Gottschling 2021
; TMS9918 screen mode 3 

i_result	equ $70 ; byte
xpos		equ $71
ypos		equ $72


Mandelbrot_Start
;	MX %11
	clc
	xce			; set native mode
	sep #$30		; accu and index 8 bit	
;	!as
;	!rs	
	ldx #0
	ldy #0
LOOP_M  
	jsr mand_get
	sta i_result
	jsr Plot
	inx
	cpx #MAND_WIDTH
	bne LOOP_M 
	ldx #0
	iny
	cpy #MAND_HEIGHT
	bne LOOP_M 
	sep #$30		; accu and index 8 bit	
	sec
	xce				; set emulation mode
	rts
	
Plot
	stx xpos
	sty ypos
	lda i_result
	jsr SETCOL
	lda ypos
	ldy xpos
	jsr PLOT
	ldx xpos
	ldy ypos
	rts	


	
