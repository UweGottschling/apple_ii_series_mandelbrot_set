**************************************************
* Apple Standard Memory Locations
**************************************************
CLRLORES          equ   $F832
LORES             equ   $C050 ;read access: enable graphic
TXTSET            equ   $C051 ;read access: enable text 
MIXCLR            equ   $C052 ;read access: disable mixed graphic or text
MIXSET            equ   $C053 ;read access: enable mixed graphic and text
TXTPAGE1          equ   $C054 ;read access: page 1 of text or graphic
TXTPAGE2          equ   $C055 ;read access: page 2 of text or graphic
KEY               equ   $C000
C80STOREOFF       equ   $C000
C80STOREON        equ   $C001
STROBE            equ   $C010
SPEAKER           equ   $C030
VBL               equ   $C02E
RDVBLBAR          equ   $C019                   ;not VBL (VBL signal low

RAMWRTAUX         equ   $C005
RAMWRTMAIN        equ   $C004
SETAN3            equ   $C05E                   ;set annunciator-3 output to 0
SET80VID          equ   $C00D                   ;enable 80-column display mode (WR-only)

COUT		equ $FDED ;call output routine whose address is stored in CSW
COUT1		equ $FDF0 ;display character on screen
RDKEY		equ $FC0C ;display blinking cursor, goes to standard input routine (KEYIN / BASICON)
KEYIN		equ $FD1B ;displays checkerboard cursor, accepr characters from keyboard	
GETLN		equ $FD6A ;displays prompt character, accept string of characters	
HOME		equ $FC58 ;clear windows and puts cursor in upper-left corner
SETGR		equ $FB40 ;set graphic in mixed mode
GR		equ $F390 ;set LORES graphic in mixed mode
SETCOL		equ $F864 ;set LORES color: reg A $0...$F
HLINE		equ $F819 ;draw horizontal line
VLINE		equ $F828 ;draw vertica line
CLRTOP		equ $F836 ;clear top 40 lines of LORES graphic1
CLRSCR		equ $F832 ;clear all 48 lines of LORES graphic1
CLRSC2		equ $F838 ;clear Y lines of LORES graphic1
PLOT		equ $F800 ;draw point at X,Y with color SETCOL: Reg A= Y, Y_Reg = X
PRBYTE		equ $FDDA ;Prints hexadecimal byte; A = hex bye to print




