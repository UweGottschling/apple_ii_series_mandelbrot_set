****************************************
*  Assembly infrastructure:            *
*  Dagen Brock <dagenbrock@gmail.com>  *
*  2014-04-17                          *
*                                      *
*  Mandelbrot from                     *
*  Matt Heffernan                      *
*  Conversion to Apple IIe             *
*  Uwe Gottschling                     *
*  2021-02-14                          *
*  CPU: 65C02, low resolution graphic  *                          
****************************************

		lst   off
		org   $2000              ; start at $2000 (all ProDOS8 system files)
		typ   $ff                ; set P8 type ($ff = "system file") for output file
		xc    off                ; @todo force 6502?
		xc    off


		dsk   mbslrc02.bin        ; tell compiler output filename

Main
		jsr StartText
		jsr SetLoresGraphic
		jsr Mandelbrot_Start

Quit                
		sta   TXTPAGE1           ; Don't forget to give them back the right page!
		ldy #1
:loop               
		lda QuitStr,y
		beq :done
		jsr COUT
		iny
		bra :loop
:done		
  		jsr KEYIN
  
                    jsr   MLI                ; first actual command, call ProDOS vector
                    dfb   $65                ; QUIT P8 request ($65)
                    da    QuitParm
                    bcs   Error
                    brk   $00                ; shouldn't ever here!
Error               brk   $00                ; shouldn't be here either

QuitParm            dfb   4                  ; number of parameters
                    dfb   0                  ; standard quit type
                    da    $0000              ; not needed when using standard quit
                    dfb   0                  ; not used
                    da    $0000              ; not used


		rts
QuitStr             str   "End",CR,"Press any key",CR,NUL



StartText	
		jsr HOME
		ldy #1
:loop               
		lda StartString,y
		beq :done
		jsr COUT
		iny
		bra :loop		
:done		
		jsr KEYIN
		rts

StartString        

		ASC CR,"Multi-mandelbrot from Matt Heffernan"
		ASC CR,"Apple Enhanced IIe version"
		ASC CR,"Uwe Gottschling"
		ASC CR,"Press any key",CR,NUL

WaitForKey  
		lda $C000   ; Wait until a key is pressed
		bpl WaitForKey
		bit $C010
		rts 

SetLoresGraphic
		jsr SETGR
		rts

MLI                 equ   $bf00              ; ProDOS entry point

		put applerom
		put ControlCHR
		put fixedpt_c02
		put mandelbrot_c02
		put mandelbrot_lores





		
		
		
				

