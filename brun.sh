#!/bin/bash
merlin32 -V . src/mbslr_c02.s
if [ $? -ne 0 ]; then
  echo "Assembly Failed.  Exiting." ;  exit 1
fi

merlin32 -V . src/mbslrFPU_c02.s
if [ $? -ne 0 ]; then
  echo "Assembly Failed.  Exiting." ;  exit 1
fi


merlin32 -V . src/mbslr.s
if [ $? -ne 0 ]; then
  echo "Assembly Failed.  Exiting." ;  exit 1
fi

merlin32 -V . src/mbslr816.s
if [ $? -ne 0 ]; then
  echo "Assembly Failed.  Exiting." ;  exit 1
fi


./make_po.sh
#gsplus
cp mbs140.po ../ADTPro-2.1.0/disks/
